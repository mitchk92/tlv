const std = @import("std");
const hex = @import("hex_to_bin.zig");
const expect = std.testing.expect;

pub const TLV = struct {
    data: []const u8,
    tag: u32,
};

test {
    const tag_test = struct {
        value: []const u8,
        values: []const TLV,
    };

    const known_tags = [_]tag_test{
        tag_test{
            .value = "9F010400112233810124",
            .values = &[_]TLV{
                TLV{ .data = "\x00\x11\x22\x33", .tag = 0x9F01 },
                TLV{ .data = "\x24", .tag = 0x81 },
            },
        },
    };

    //old
    //tag_test{ .value = 0x9F01, .encoded = "9F01" },
    //  tag_test{ .value = 0x9F40, .encoded = "9F40" },
    // tag_test{ .value = 0x81, .encoded = "81" },
    //tag_test{.value = 0xtag_test{.value = 0xg, .encoded = tag_test{ .value = 0x9F02, .encoded = "9F02" }},
    //tag_test{ .value = 0x9F04, .encoded = "9F04" },
    //tag_test{ .value = 0x9F4A, .encoded = "9F4A" },
    //  tag_test{ .value = 0x9F42, .encoded = "9F42" },
    //  tag_test{ .value = 0xDFDF22, .encoded = "DFDF22" },
    //tag_test{ .value = 0x94, .encoded = "94" },
    //tag_test{ .value = 0x50, .encoded = "50" },
    //tag_test{ .value = 0x5f34, .encoded = "5f34" },
    //tag_test{ .value = 0x8C, .encoded = "8C" },
    //tag_test{ .value = 0xBF0C, .encoded = "BF0C" },
    std.debug.print("\n\n", .{});
    var buff: [256]u8 = undefined;
    for (known_tags) |tag| {
        const res = try hex.ascii_hex_to_bin_arr(tag.value, buff[0..]);
        var iter = TLV_iterator.init(res);
        var idx: usize = 0;
        while (try iter.next()) |item| {
            try expect(tag.values[idx].tag == item.tag);
            try expect(std.mem.eql(u8, tag.values[idx].data, item.data));
            idx += 1;
        }

        //const val = try TLV_iterator.decode_tag(res);
        //try expect(tag.value == val);
    }
}

pub const TLV_iterator = struct {
    data: []const u8,
    i: usize,
    pub fn init(data: []const u8) TLV_iterator {
        return TLV_iterator{
            .data = data,
            .i = 0,
        };
    }
    fn decode_tag(self: *TLV_iterator) !u32 {
        if (self.data.len <= self.i) {
            return error.NoMoreData;
        }
        const data_start = self.data[self.i];
        if (data_start & 0xF == 0xF) {
            const data_cont = self.data[self.i + 1];
            if (data_cont & (1 << 7) != 0) {
                const tag = @intCast(u32, self.data[self.i + 2]) | @intCast(u32, self.data[self.i + 1]) << 8 | @intCast(u32, self.data[self.i + 0]) << 16;
                //std.debug.print("[{s} {x}]\n\n", .{ std.fmt.fmtSliceHexLower(self.data[self.i .. self.i + 5]), tag });
                self.i += 3;
                return tag;
                //three bytes
            } else {
                self.i += 2;
                return @intCast(u32, data_cont) | @as(u32, data_start) << 8;
            }
        } else {
            self.i += 1;
            return @intCast(u32, data_start);
        }
    }

    fn decode_len(self: *TLV_iterator) !usize {
        if (self.data.len <= self.i) return error.NoMoreData;
        const val = @truncate(u7, self.data[self.i + 0]);

        if (self.data[self.i] & (1 << 7) == 0) {
            self.i += 1;
            return @intCast(usize, val);
            //single byte length
        } else {
            if (val > @sizeOf(usize)) {
                const bytes = std.mem.readVarInt(usize, self.data[self.i + 1 .. self.i + val], .Big);
                self.i += val + 1;
                return bytes;
            } else {
                return error.LengthTooLongForNative;
            }
            //multibyte length
        }
    }

    pub fn next(self: *TLV_iterator) !?TLV {
        const tag = self.decode_tag() catch |err| switch (err) {
            error.NoMoreData => {
                return null;
            },
            else => |e| {
                return e;
            },
        };
        const len = self.decode_len() catch |err| switch (err) {
            error.NoMoreData => {
                return error.InvalidTrailingTag;
            },
            else => |e| {
                return e;
            },
        };
        const data = self.data[self.i .. self.i + len];
        self.i += len;
        return TLV{
            .data = data,
            .tag = tag,
        };
    }
};
